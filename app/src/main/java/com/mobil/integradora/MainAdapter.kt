package com.mobil.integradora

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobil.integradora.mvvm.data.Autoridades
import kotlinx.android.synthetic.main.item_autoridades.view.cardRest
import kotlinx.android.synthetic.main.item_autoridades.view.descripcion
import kotlinx.android.synthetic.main.item_autoridades.view.imagenUrl
import kotlinx.android.synthetic.main.item_autoridades.view.nombre

class MainAdapter(private val context: Context) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    private var dataList = mutableListOf<Autoridades>()

    fun setListData(data:MutableList<Autoridades>){
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_autoridades, parent,false)

        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if(dataList.size>0){
            dataList.size
        }else{
            0
        }
    }

    private val idToActivityMap: Map<String, Class<out Activity>> = mapOf(
        "1" to MagistradosActivity::class.java,
        "2" to JuecesActivity::class.java,
        "3" to InstanciasActivity::class.java,
        "4" to LaboralesActivity::class.java,
        // Agrega más mapeos según sea necesario
    )

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val autoridad: Autoridades = dataList[position]
        holder.bindView(autoridad)

        // Agrega clic a la tarjeta (CardView)
        holder.itemView.cardRest.setOnClickListener {
            val autoridadId = autoridad.id.toString() // Convierte el ID a String

            // Verifica si el ID está mapeado a una actividad
            if (idToActivityMap.containsKey(autoridadId)) {
                val activityClass = idToActivityMap[autoridadId]
                val intent = Intent(context, activityClass)
                context.startActivity(intent)
            } else {
                // Manejo predeterminado si no se encuentra una actividad mapeada
            }
        }
    }



    inner class MainViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        fun bindView(autoridades: Autoridades){
           Glide.with(context).load(autoridades.imagenUrl).into(itemView.imagenUrl)
            itemView.nombre.text = autoridades.nombre
            itemView.descripcion.text = autoridades.descripcion
        }
    }


}