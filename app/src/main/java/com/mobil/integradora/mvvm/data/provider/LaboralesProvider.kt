package com.mobil.integradora.mvvm.data.provider

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.mobil.integradora.mvvm.data.Salas

class LaboralesProvider {

    fun getLaboralesData(): LiveData<MutableList<Salas>> {
        val mutableData = MutableLiveData<MutableList<Salas>>()

        FirebaseFirestore.getInstance().collection("Sala-Laborales")
            .get()
            .addOnSuccessListener { querySnapshot ->
                val listData: MutableList<Salas> = mutableListOf()
                for (document in querySnapshot) {
                    val id : String? = document.getString("id")
                    val nombre: String? = document.getString("nombre")
                    val salas = Salas(id?: "", nombre ?: "")
                    listData.add(salas)
                }
                mutableData.value = listData
            }
            .addOnFailureListener { exception ->
                // Maneja errores aquí si es necesario
                // En esta función, estás obteniendo todos los datos de "Autoridades" sin ningún filtro
            }

        return mutableData
    }

}