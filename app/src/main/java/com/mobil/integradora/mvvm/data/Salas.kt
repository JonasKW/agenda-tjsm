package com.mobil.integradora.mvvm.data

data class Salas (
    val id:String = "",
    val nombre:String = "",
    val contactos: List<Contacto> = emptyList(),
)