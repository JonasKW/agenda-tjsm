package com.mobil.integradora.mvvm.data

data class Contacto(
    val id: String = "",
    val cargo: String = "",
    val codigo: String = "",
    val direccion: String = "",
    val ext: String = "",
    val imagenUrl: String = "",
    val nombre: String = "",
    val ponencia: String = "",
    val telefono: String = ""
)