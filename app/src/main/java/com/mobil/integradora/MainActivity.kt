package com.mobil.integradora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobil.integradora.databinding.ActivityMainBinding
import com.mobil.integradora.mvvm.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    private lateinit var adapter: MainAdapter
    private val viewModel: MainViewModel by lazy { ViewModelProvider(this).get(MainViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Inicializa el adaptador
        adapter = MainAdapter(this)

        // Obtén una referencia al EditText
        val text = binding.text
        val searchEditText = binding.search
        val emptyText = binding.emptyTextView



        // Obtén una referencia al RecyclerView desde la vista inflada mediante View Binding
        val recyclerView = binding.list

        // Configura el RecyclerView con un LinearLayoutManager
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Configura el RecyclerView con el adaptador
        recyclerView.adapter = adapter

        // Observa los datos de las "Autoridades"
        viewModel.fetchAutoridadesData().observe(this, Observer { autoridadesList ->
            // Imprimir los datos en el logcat
            Log.d("FirebaseData", "Datos recibidos: $autoridadesList")

            // Actualizar el adaptador con los datos
            adapter.setListData(autoridadesList)
            adapter.notifyDataSetChanged()
        })

        // Agrega un TextWatcher al EditText para realizar búsquedas en tiempo real
        searchEditText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val query = s.toString()
                Log.d("MainActivity", "Texto de búsqueda: $query") // Agrega esta línea para verificar el texto de búsqueda

                // Realizar búsqueda y actualizar la lista en función de la consulta
                viewModel.searchData(query).observe(this@MainActivity, Observer {
                    Log.d("MainActivity", "Resultados de búsqueda: $it") // Agrega esta línea para verificar los resultados
                    // Actualizar el adaptador con los datos filtrados
                    adapter.setListData(it)
                    adapter.notifyDataSetChanged()
                    // Muestra u oculta el TextView según si se encuentran datos
                    if (it.isEmpty()) {
                        emptyText.visibility = View.VISIBLE  // Mostrar el mensaje si no hay datos
                    } else {
                        emptyText.visibility = View.GONE     // Ocultar el mensaje si hay datos
                    }
                })
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })
    }

}



