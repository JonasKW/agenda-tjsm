package com.mobil.integradora

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.mobil.integradora.mvvm.data.Salas
import kotlinx.android.synthetic.main.item_salas.view.nombre

class SalaAdapter(private val context: Context) : RecyclerView.Adapter<SalaAdapter.MainViewHolder>() {
    private var dataList = mutableListOf<Salas>()

    fun setListData(data:MutableList<Salas>){
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_salas, parent,false)

        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if(dataList.size>0){
            dataList.size
        }else{
            0
        }
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val salas: Salas = dataList[position]
        holder.bindView(salas)
    }

    inner class MainViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun bindView(salas: Salas){
            itemView.nombre.text = salas.nombre
        }
    }

}