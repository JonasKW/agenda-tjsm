package com.mobil.integradora.mvvm.data

data class User (
    var id_auth: String?,
    var name: String,
    var password: String,
    var phone: String,
    var email: String,
    )