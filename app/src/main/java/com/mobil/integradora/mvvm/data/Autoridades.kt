package com.mobil.integradora.mvvm.data

data class Autoridades (
    val id:String = "",
    val imagenUrl:String = "",
    val nombre:String = "",
    val descripcion:String = ""
)
