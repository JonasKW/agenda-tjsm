package com.mobil.integradora

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobil.integradora.mvvm.data.Salas
import kotlinx.android.synthetic.main.item_autoridades.view.cardRest
import kotlinx.android.synthetic.main.item_salas.view.nombre

class MagistradosAdapter(private val context: Context) : RecyclerView.Adapter<MagistradosAdapter.MainViewHolder>() {

    private var dataList = mutableListOf<Salas>()

    fun setListData(data:MutableList<Salas>){
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_salas, parent,false)

        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if(dataList.size>0){
            dataList.size
        }else{
            0
        }
    }

    private val idToActivityMap: Map<String, Class<out Activity>> = mapOf(
        "1" to ContactoMagistradosActivity::class.java,
    )

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val salamagistrados: Salas = dataList[position]
        holder.bindView(salamagistrados)

        // Agrega clic a la tarjeta (CardView)
        holder.itemView.cardRest.setOnClickListener {
            val autoridadId = salamagistrados.id.toString() // Convierte el ID a String

            // Verifica si el ID está mapeado a una actividad
            if (idToActivityMap.containsKey(autoridadId)) {
                val activityClass = idToActivityMap[autoridadId]
                val intent = Intent(context, activityClass)
                context.startActivity(intent)
            } else {
                // Manejo predeterminado si no se encuentra una actividad mapeada
            }
        }
    }

    inner class MainViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun bindView(salas: Salas){
            itemView.nombre.text = salas.nombre
        }
    }
}