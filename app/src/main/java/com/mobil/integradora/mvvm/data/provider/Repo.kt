package com.mobil.integradora.mvvm.data.provider

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.mobil.integradora.mvvm.data.Autoridades

class Repo {
    fun getAutoridadesData(): LiveData<MutableList<Autoridades>> {
        val mutableData = MutableLiveData<MutableList<Autoridades>>()

        FirebaseFirestore.getInstance().collection("Autoridades")
            .get()
            .addOnSuccessListener { querySnapshot ->
                val listData: MutableList<Autoridades> = mutableListOf()
                for (document in querySnapshot) {
                    val id : String? = document.getString("id")
                    val imagenUrl: String? = document.getString("imagenUrl")
                    val nombre: String? = document.getString("nombre")
                    val descripcion: String? = document.getString("descripcion")
                    val autoridades = Autoridades(id?: "",imagenUrl ?: "", nombre ?: "", descripcion ?: "")
                    listData.add(autoridades)
                }
                mutableData.value = listData
            }
            .addOnFailureListener { exception ->
                // Maneja errores aquí si es necesario
                // En esta función, estás obteniendo todos los datos de "Autoridades" sin ningún filtro
            }

        return mutableData
    }
}
