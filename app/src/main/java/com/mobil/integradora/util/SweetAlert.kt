package com.mobil.integradora.util

import android.content.Context
import cn.pedant.SweetAlert.SweetAlertDialog

object SweetAlert{


    fun swAlertWarn(tittleMessage: String?, contentMessage: String?,confirmText:String,context: Context) {
        SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
            .setTitleText(tittleMessage)
            .setContentText(contentMessage)
            .setConfirmText(confirmText)
            .show()
    }

    fun swAlertError(tittleMessage: String?, contentMessage: String?,context: Context) {
        SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
            .setTitleText(tittleMessage)
            .setContentText(contentMessage)
            .show()
    }

    fun swAlertSuccess(tittleMessage: String?, contentMessage: String?,context: Context) {
        SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
            .setTitleText(tittleMessage)
            .setContentText(contentMessage)
            .show()
    }

    fun swAlertNormal(tittleMessage: String?, contentMessage: String?,context: Context) {
        SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
            .setTitleText(tittleMessage)
            .setContentText(contentMessage)
        .show()
    }


}