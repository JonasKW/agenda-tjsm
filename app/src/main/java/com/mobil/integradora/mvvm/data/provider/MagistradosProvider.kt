package com.mobil.integradora.mvvm.data.provider

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.mobil.integradora.mvvm.data.Contacto
import com.mobil.integradora.mvvm.data.Salas

class MagistradosProvider {

    fun getMagistradosData(): LiveData<MutableList<Salas>> {
        val mutableData = MutableLiveData<MutableList<Salas>>()

        FirebaseFirestore.getInstance().collection("Sala-Magistrados")
            .get()
            .addOnSuccessListener { querySnapshot ->
                val listData: MutableList<Salas> = mutableListOf()
                for (document in querySnapshot) {
                    val id : String? = document.getString("id")
                    val nombre: String? = document.getString("nombre")
                    val salas = Salas(id?: "", nombre ?: "")
                    listData.add(salas)
                }
                mutableData.value = listData
            }
            .addOnFailureListener { exception ->
                // Maneja errores aquí si es necesario
                // En esta función, estás obteniendo todos los datos de "Autoridades" sin ningún filtro
            }

        return mutableData
    }

    fun getMagistradosContactoData(): LiveData<MutableList<Salas>> {
        val mutableData = MutableLiveData<MutableList<Salas>>()

        FirebaseFirestore.getInstance().collection("Sala-Magistrados")
            .get()
            .addOnSuccessListener { querySnapshot ->
                val listData: MutableList<Salas> = mutableListOf()
                for (document in querySnapshot) {
                    val contactosList = document.get("contactos") as? List<HashMap<String, Any>>
                    val contactos = contactosList?.map { contactoData ->
                        Contacto(
                            id = contactoData["id"] as? String ?: "",
                            cargo = contactoData["cargo"] as? String ?: "",
                            codigo = contactoData["codigo"] as? String ?: "",
                            direccion = contactoData["direccion"] as? String ?: "",
                            ext = contactoData["ext"] as? String ?: "",
                            imagenUrl = contactoData["imagenUrl"] as? String ?: "",
                            nombre = contactoData["nombre"] as? String ?: "",
                            ponencia = contactoData["ponencia"] as? String ?: "",
                            telefono = contactoData["telefono"] as? String ?: ""
                        )
                    } ?: emptyList()

                    val salas = Salas("", "", contactos) // No incluyas "id" ni "nombre"
                    listData.add(salas)
                }
                mutableData.value = listData
            }
            .addOnFailureListener { exception ->
                // Maneja errores aquí si es necesario
            }

        return mutableData
    }

}