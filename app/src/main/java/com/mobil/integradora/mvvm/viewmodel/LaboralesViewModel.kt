package com.mobil.integradora.mvvm.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.mobil.integradora.mvvm.data.Salas
import com.mobil.integradora.mvvm.data.provider.LaboralesProvider

class LaboralesViewModel : ViewModel() {
    private val laborales = LaboralesProvider()

    // Obtener todos los datos de las "Salas"
    fun fetchLaboralesData(): LiveData<MutableList<Salas>> {
        return laborales.getLaboralesData()
    }

    fun searchData(query: String): LiveData<MutableList<Salas>> {
        val mutableData = MutableLiveData<MutableList<Salas>>()

        // Si el campo de búsqueda está vacío, devuelve todos los datos sin aplicar ninguna condición de búsqueda
        if (query.isEmpty()) {
            fetchLaboralesData().observeForever {
                mutableData.value = it
            }
        } else {
            // Realiza la consulta en Firebase para filtrar los datos según el criterio de búsqueda (query)
            // Supongamos que tienes una referencia a la colección "autoridades" en Firebase
            val queryRef = FirebaseFirestore.getInstance().collection("Sala-Laborales")
                .whereEqualTo(
                    "nombre",
                    query
                ) // Ajusta "campo_de_busqueda" al campo que deseas buscar

            queryRef.get().addOnSuccessListener { documents ->
                val filteredList = mutableListOf<Salas>()
                for (document in documents) {
                    // Convierte los documentos de Firebase en objetos Autoridades y agrégalos a la lista filtrada
                    val salas = document.toObject(Salas::class.java)
                    filteredList.add(salas)
                }
                mutableData.value = filteredList
            }.addOnFailureListener { exception ->
                // Maneja los errores de consulta aquí, si es necesario
                Log.e("MainViewModel", "Error al buscar datos: $exception")
            }
        }

        return mutableData
    }
}