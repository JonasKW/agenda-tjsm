package com.mobil.integradora.mvvm.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.mobil.integradora.mvvm.data.Autoridades
import com.mobil.integradora.mvvm.data.Salas
import com.mobil.integradora.mvvm.data.provider.MagistradosProvider

class MagistradosViewModel : ViewModel() {
    private val magistrados = MagistradosProvider()

    // Obtener todos los datos de las "Salas" con y sin contactos
    fun fetchMagistradosData(): LiveData<MutableList<Salas>> {
        return magistrados.getMagistradosData()
    }

    // Buscar datos de "Salas" por nombre
    fun searchData(query: String): LiveData<MutableList<Salas>> {
        val mutableData = MutableLiveData<MutableList<Salas>>()

        if (query.isEmpty()) {
            // Si el campo de búsqueda está vacío, devuelve todos los datos
            fetchMagistradosData().observeForever {
                mutableData.value = it
            }
        } else {
            // Realiza la consulta en Firebase para filtrar los datos por nombre
            val queryRef = FirebaseFirestore.getInstance().collection("Sala-Magistrados")
                .whereEqualTo("nombre", query)

            queryRef.get().addOnSuccessListener { documents ->
                val filteredList = mutableListOf<Salas>()
                for (document in documents) {
                    val salas = document.toObject(Salas::class.java)
                    filteredList.add(salas)
                }
                mutableData.value = filteredList
            }.addOnFailureListener { exception ->
                // Maneja los errores de consulta aquí, si es necesario
                Log.e("MainViewModel", "Error al buscar datos: $exception")
            }
        }

        return mutableData
    }

    // Obtener todos los datos de "Salas" con contactos
    fun fetchMagistradosContactoData(): LiveData<MutableList<Salas>> {
        return magistrados.getMagistradosContactoData()
    }

    // Buscar datos de "Salas" con contactos por nombre
    fun searchContactoData(query: String): LiveData<MutableList<Salas>> {
        val mutableData = MutableLiveData<MutableList<Salas>>()

        if (query.isEmpty()) {
            // Si el campo de búsqueda está vacío, devuelve todos los datos con contactos
            fetchMagistradosContactoData().observeForever {
                mutableData.value = it
            }
        } else {
            // Realiza la consulta en Firebase para filtrar los datos por nombre
            val queryRef = FirebaseFirestore.getInstance().collection("Sala-Magistrados")
                .whereEqualTo("nombre", query)

            queryRef.get().addOnSuccessListener { documents ->
                val filteredList = mutableListOf<Salas>()
                for (document in documents) {
                    val salas = document.toObject(Salas::class.java)
                    filteredList.add(salas)
                }
                mutableData.value = filteredList
            }.addOnFailureListener { exception ->
                // Maneja los errores de consulta aquí, si es necesario
                Log.e("MainViewModel", "Error al buscar datos: $exception")
            }
        }

        return mutableData
    }
}
