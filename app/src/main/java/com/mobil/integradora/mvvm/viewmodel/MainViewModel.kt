package com.mobil.integradora.mvvm.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.mobil.integradora.mvvm.data.provider.Repo
import com.mobil.integradora.mvvm.data.Autoridades

class MainViewModel : ViewModel() {
    private val repo = Repo()

    // Obtener todos los datos de las "Autoridades"
    fun fetchAutoridadesData(): LiveData<MutableList<Autoridades>> {
        return repo.getAutoridadesData()
    }

    fun searchData(query: String): LiveData<MutableList<Autoridades>> {
        val mutableData = MutableLiveData<MutableList<Autoridades>>()

        // Si el campo de búsqueda está vacío, devuelve todos los datos sin aplicar ninguna condición de búsqueda
        if (query.isEmpty()) {
            fetchAutoridadesData().observeForever {
                mutableData.value = it
            }
        } else {
            // Realiza la consulta en Firebase para filtrar los datos según el criterio de búsqueda (query)
            // Supongamos que tienes una referencia a la colección "autoridades" en Firebase
            val queryRef = FirebaseFirestore.getInstance().collection("Autoridades")
                .whereEqualTo("nombre", query) // Ajusta "campo_de_busqueda" al campo que deseas buscar

            queryRef.get().addOnSuccessListener { documents ->
                val filteredList = mutableListOf<Autoridades>()
                for (document in documents) {
                    // Convierte los documentos de Firebase en objetos Autoridades y agrégalos a la lista filtrada
                    val autoridad = document.toObject(Autoridades::class.java)
                    filteredList.add(autoridad)
                }
                mutableData.value = filteredList
            }.addOnFailureListener { exception ->
                // Maneja los errores de consulta aquí, si es necesario
                Log.e("MainViewModel", "Error al buscar datos: $exception")
            }
        }

        return mutableData
    }
}
